<?php

namespace Officient\ApiEssentials;

use Officient\ApiEssentials\Exception\UnexpectedTypeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * Class JsonRequest
 * @package Officient\ApiEssentials
 */
class JsonRequest
{
    /**
     * @var array
     */
    private $data;

    /**
     * JsonRequest constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->data = json_decode($request->getContent(), true);
        if(is_array($this->data) === false) {
            $this->data = array();
        }
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed|null
     */
    public function get(string $key, $default = null)
    {
        return $this->data[$key] ?? $default;
    }

    /**
     * @param string $key
     * @param bool $testNull
     * @return bool
     */
    public function has(string $key, bool $testNull = true): bool
    {
        if($testNull) {
            return isset($this->data[$key]);
        } else {
            return array_key_exists($key, $this->data);
        }
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->data;
    }

    public function updateObject($object)
    {
        if(!is_object($object)) {
            throw new UnexpectedTypeException($object, 'object');
        }

        $converter = new CamelCaseToSnakeCaseNameConverter();

        $reflection = new \ReflectionClass($object);
        foreach ($this->data as $key => $value) {
            $propertyName = $key;
            if(!$reflection->hasProperty($propertyName)) {
                $propertyName = $converter->denormalize($propertyName);
            }
            if(!$reflection->hasProperty($propertyName)) {
                continue;
            }

            $value = $this->convertValue($value);

            $setterMethod = "set".ucfirst($propertyName);
            if($reflection->hasMethod($setterMethod)) {
                $object->{$setterMethod}($value);
            } else if($reflection->getProperty($propertyName)->isPublic()) {
                $object->{$propertyName} = $value;
            }
        }
    }

    /**
     * @param $value
     * @return mixed
     */
    public function convertValue($value)
    {
        if(is_string($value)) {
            try {
                return new \DateTime($value);
            } catch (\Exception $e) {}
        }

        return $value;
    }
}