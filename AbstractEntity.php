<?php

namespace Officient\ApiEssentials;

/**
 * Class AbstractEntity
 * @package Officient\ApiEssentials
 */
abstract class AbstractEntity implements \JsonSerializable
{
    protected $includeJson = [];

    /**
     * @return array|mixed
     * @throws \Exception
     */
    public function jsonSerialize(): mixed
    {
        $data = array();
        foreach($this->includeJson as $field) {
            $method = $field;
            if(method_exists($this, $method) === false) {
                $method = "get".ucfirst($field);
                if(method_exists($this, $method) === false) {
                    throw new \Exception("Field {$field} has not getter");
                }
            }

            $data[$field] = $this->{$method}();
        }
        return $data;
    }
}