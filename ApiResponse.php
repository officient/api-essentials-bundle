<?php

namespace Officient\ApiEssentials;

use Symfony\Component\HttpFoundation\JsonResponse;
use Throwable;

/**
 * Class ApiResponse
 * @package Officient\ApiEssentials
 */
class ApiResponse extends JsonResponse
{
    // Codes
    const CODE_OK                   = 1000;
    const CODE_NOT_FOUND            = 1001;
    const CODE_VALIDATION_ERROR     = 1002;
    const CODE_ALREADY_EXISTS       = 1003;
    const CODE_NON_UNIQUE_RESULT    = 1004;
    const CODE_NO_RESULT            = 1005;
    const CODE_EXCEPTION            = 1006;
    const CODE_ERROR                = 1007;

    // Messages
    const MSG_OK                    = 'Ok';
    const MSG_NOT_FOUND             = 'Not found';
    const MSG_VALIDATION_ERROR      = 'Some fields did not pass validation. See result';
    const MSG_ALREADY_EXISTS        = 'Already exists';
    const MSG_NON_UNIQUE_RESULT     = 'Non unique result found. Expected 1 record, got multiple';
    const MSG_NO_RESULT             = 'No result found. Expected 1 record, got none';
    const MSG_EXCEPTION             = 'External exception thrown';
    const MSG_ERROR                 = 'An error occurred. See result';

    /**
     * ApiResponse constructor.
     * @param string $message
     * @param int $code
     * @param null $result
     * @param int $status
     * @param array $headers
     * @param bool $json
     */
    public function __construct(string $message, int $code, $result = null, int $status = self::HTTP_OK, array $headers = [], bool $json = false)
    {
        parent::__construct([
            'message' => $message,
            'code' => $code,
            'result' => $result
        ], $status, $headers, $json);
    }

    /**
     * @param null $result
     * @param int $status
     * @param array $headers
     * @param bool $json
     * @return ApiResponse
     */
    public static function ok($result = null, int $status = self::HTTP_OK, array $headers = [], bool $json = false): ApiResponse
    {
        return new ApiResponse(self::MSG_OK, self::CODE_OK, $result, $status, $headers, $json);
    }

    /**
     * @param null $result
     * @param int $status
     * @param array $headers
     * @param bool $json
     * @return ApiResponse
     */
    public static function notFound($result = null, int $status = self::HTTP_NOT_FOUND, array $headers = [], bool $json = false): ApiResponse
    {
        return new ApiResponse(self::MSG_NOT_FOUND, self::CODE_NOT_FOUND, $result, $status, $headers, $json);
    }

    /**
     * @param null $result
     * @param int $status
     * @param array $headers
     * @param bool $json
     * @return ApiResponse
     */
    public static function validationError($result = null, int $status = self::HTTP_BAD_REQUEST, array $headers = [], bool $json = false): ApiResponse
    {
        return new ApiResponse(self::MSG_VALIDATION_ERROR, self::CODE_VALIDATION_ERROR, $result, $status, $headers, $json);
    }

    /**
     * @param null $result
     * @param int $status
     * @param array $headers
     * @param bool $json
     * @return ApiResponse
     */
    public static function alreadyExists($result = null, int $status = self::HTTP_CONFLICT, array $headers = [], bool $json = false): ApiResponse
    {
        return new ApiResponse(self::MSG_ALREADY_EXISTS, self::CODE_ALREADY_EXISTS, $result, $status, $headers, $json);
    }

    /**
     * @param null $result
     * @param int $status
     * @param array $headers
     * @param bool $json
     * @return ApiResponse
     */
    public static function nonUniqueResult($result = null, int $status = self::HTTP_CONFLICT, array $headers = [], bool $json = false): ApiResponse
    {
        return new ApiResponse(self::MSG_NON_UNIQUE_RESULT, self::CODE_NON_UNIQUE_RESULT, $result, $status, $headers, $json);
    }

    /**
     * @param null $result
     * @param int $status
     * @param array $headers
     * @param bool $json
     * @return ApiResponse
     */
    public static function noResult($result = null, int $status = self::HTTP_NOT_FOUND, array $headers = [], bool $json = false): ApiResponse
    {
        return new ApiResponse(self::MSG_NO_RESULT, self::CODE_NO_RESULT, $result, $status, $headers, $json);
    }

    /**
     * @param Throwable $throwable
     * @param int $status
     * @param array $headers
     * @param bool $json
     * @return ApiResponse
     * @throws \ReflectionException
     */
    public static function exception(Throwable $throwable, int $status = self::HTTP_INTERNAL_SERVER_ERROR, array $headers = [], bool $json = false): ApiResponse
    {
        $reflection = new \ReflectionClass($throwable);
        return new ApiResponse(self::MSG_EXCEPTION, self::CODE_EXCEPTION, [
            'exception' => $reflection->getShortName(),
            'namespace' => $reflection->getNamespaceName(),
            'line' => $throwable->getLine(),
            'file' => $throwable->getFile(),
            'message' => $throwable->getMessage(),
            'code' => $throwable->getCode(),
            'stackTraceString' => $throwable->getTraceAsString(),
            'stackTrace' => $throwable->getTrace()
        ], $status, $headers, $json);
    }

    /**
     * @param null $result
     * @param int $status
     * @param array $headers
     * @param bool $json
     * @return ApiResponse
     */
    public static function error($result = null, int $status = self::HTTP_INTERNAL_SERVER_ERROR, array $headers = [], bool $json = false): ApiResponse
    {
        return new ApiResponse(self::MSG_ERROR, self::CODE_ERROR, $result, $status, $headers, $json);
    }
}