<?php

namespace Officient\ApiEssentials;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Officient\OrmEssentials\Criterion;

/**
 * Class AbstractRepository
 * @package Officient\ApiEssentials
 */
abstract class AbstractRepository extends ServiceEntityRepository
{
    /**
     * @param array $criteria
     * @param null $limit
     * @param null $offset
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countBy(array $criteria, $limit = null, $offset = null): int
    {
        $builder = $this->createQueryBuilder('n');
        $builder->select('COUNT(n)');
        $this->processCriteria($builder, $criteria);
        $builder->setFirstResult($offset);
        $builder->setMaxResults($limit);

        $query = $builder->getQuery();
        return $query->getSingleScalarResult();
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return array
     * @throws Exception
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        $builder = $this->createQueryBuilder('n');
        $this->processCriteria($builder, $criteria);
        $this->processOrderBy($builder, $orderBy);
        $builder->setMaxResults($limit);
        $builder->setFirstResult($offset);

        $query = $builder->getQuery();
        return $query->getResult();
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return null|object
     * @throws Exception
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        $builder = $this->createQueryBuilder('n');
        $this->processCriteria($builder, $criteria);
        $this->processOrderBy($builder, $orderBy);

        $query = $builder->getQuery();
        return $query->getSingleResult();
    }

    /**
     * @param QueryBuilder $builder
     * @param array $criteria
     * @throws Exception
     */
    protected function processCriteria(QueryBuilder $builder, array $criteria)
    {
        foreach ($criteria as $field => $value) {
            if(is_scalar($value)) {
                $criterion = new Criterion($field, '=', $value);
                $criterion->andWhere($builder);
            } else if($value instanceof Criterion) {
                $criterion = $value;
                $criterion->andWhere($builder);
            } else if(is_array($value)) {
                $arr = $value;
                $value1 = isset($arr['value1']) ? $arr['value1'] : null;
                $value2 = isset($arr['value2']) ? $arr['value2'] : null;
                $criterion = new Criterion($value['field'], $value['operator'], $value1, $value2);
                $criterion->andWhere($builder);
            }
        }
    }

    /**
     * @param QueryBuilder $builder
     * @param array|null $orderBy
     */
    protected  function processOrderBy(QueryBuilder $builder, array $orderBy = null)
    {
        $alias = $builder->getAllAliases()[0];
        if(is_array($orderBy) === true) {
            foreach($orderBy as $field => $direction) {
                $builder->addOrderBy("{$alias}.{$field}", $direction);
            }
        }
    }
}