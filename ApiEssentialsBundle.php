<?php

namespace Officient\ApiEssentials;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ApiEssentialsBundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\ApiEssentials
 */
class ApiEssentialsBundle extends Bundle
{

}