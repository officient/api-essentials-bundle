<?php

namespace Officient\ApiEssentials\Exception;

class UnexpectedTypeException extends \RuntimeException
{
    public function __construct($value, string $expectedType)
    {
        parent::__construct("Unexpected type '".gettype($value)."'. Expected '".$expectedType."'");
    }
}