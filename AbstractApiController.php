<?php

namespace Officient\ApiEssentials;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractApiController
 * @package Officient\ApiEssentials
 */
abstract class AbstractApiController extends AbstractController
{
    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * AbstractApiController constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }
    /**
     * @param JsonRequest $request
     * @param Collection $constraints
     * @return array
     */
    protected function validate(JsonRequest $request, Collection $constraints): array
    {
        $violations = $this->validator->validate($request->all(), $constraints);
        return $this->getViolationsArray($violations);
    }

    /**
     * @param $violations
     * @return array
     */
    protected function getViolationsArray(ConstraintViolationListInterface $violations): array
    {
        $violationMessages = [];
        foreach ($violations as $key => $violation) {
            $violationMessages[$violation->getPropertyPath()][] = $violation->getMessage();
        }
        return $violationMessages;
    }
}