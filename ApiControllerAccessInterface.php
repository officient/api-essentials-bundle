<?php

namespace Officient\ApiEssentials;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface ApiControllerAccessInterface
 * @package Officient\ApiEssentials
 */
interface ApiControllerAccessInterface
{
    /**
     * @param Request $request
     * @return ApiResponse
     */
    public function countBy(Request $request): ApiResponse;

    /**
     * @param Request $request
     * @return ApiResponse
     */
    public function findBy(Request $request): ApiResponse;

    /**
     * @param Request $request
     * @return ApiResponse
     */
    public function findOneBy(Request $request): ApiResponse;
}